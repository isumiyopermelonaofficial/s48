import React from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import { Container } from 'react-bootstrap';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Error404 from './pages/Error404.js';

// For Routes
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

// The Router(BrowserRouter) component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser

// The Routes(before it was called Switch) declares the Route we can go to.

function App() {
  return (
  <Router>
   <AppNavbar />
   <Container>
    <Routes>
   	  < Route path="/" element={ <Home /> }/>
      < Route path="/courses" element={ <Courses /> }/>
      < Route path="/login" element={ <Login /> }/>
      < Route path="/register" element={ <Register /> }/>
      < Route path="/logout" element={ <Logout /> }/>
      < Route path="*" element={<Error404 />} />
    </Routes>
   </Container>
  </Router>
  );
}

export default App;