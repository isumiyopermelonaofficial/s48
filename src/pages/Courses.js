import React from 'react';
import coursesData from '../mockData/coursesData.js';
import Course from '../components/Course.js';

export default function Courses() {
	// Displaying all the courses from the data file. .map is needed to loop through all the data
	const courses = coursesData.map(course => {
		return(
			<Course key={course.id} courseProp={course}/>
		)
	});

	return (
		<>
		<h1>Courses</h1>
		{courses}
		</>
	)
}