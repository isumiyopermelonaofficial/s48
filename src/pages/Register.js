import React, {useState, useEffect} from 'react';
import {Form,Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Register() {
	
	// State hooks
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');


	// Conditional Rendering
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both password match
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [email, password1, password2])

	function registerUser(e) {
		e.preventDefault();
		clearFields()
		//alert('Thank you for Registering!');
		Swal.fire({
			title: "Registered!",
			icon: "success",
			text: "You have Successfully Registered"
		})
	}

	function clearFields() {
		setEmail('');
		setPassword1('');
		setPassword2('')
	}

	return (
		<Form className="mt-3" onSubmit={(e) => registerUser(e)}>
			<h1>Register</h1>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)}
				/>
			</Form.Group>
			{isActive ?
			<Button className="mt-2" variant="primary" type="submit">Submit</Button>
			:
			<Button className="mt-2" variant="primary" type="submit" disabled>Submit</Button>
			}
			
		</Form>
	)
}