import React, {useState, useEffect} from 'react';
import {Card,Row,Col,Button} from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function Course({courseProp}) {
	const {name, description, price} = courseProp

	/*
		Syntax:
		const [currentValues(getter), updateValue(setter)] = useState(InitialGetterValue) 
	*/ 
	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);
	//console.log(count);

	const [isOpen, setIsOpen] = useState(true);

	const enroll = () => {	
		// //console.log("Enrollees: " + count);
		// if(count !== 30){
		// 	setCount(count + 1);
		// } else {
		// 	return;
		// }
		setCount(count + 1);
		setSeats(seats - 1);
		//console.log("Enrollees:")
	}

	useEffect(() => {
		if(seats === 0) {
			setIsOpen(false)
		}
	}, [seats])

	return (
		<Row className="mt-2">
			<Col xs={12} md={12}>
				<Card className="cardCourse mt-2">
					<Card.Body>
						<Card.Title><h2>{name}</h2></Card.Title>
						<Card.Text>
							<p> <b>Description:</b> <br />
							   {description}
							</p>

							<p> <b>Price:</b> <br />
							   {price}
							</p>
							<Card.Text>Enrollees: {count}</Card.Text>
							<Card.Text>Available Seats: {seats}</Card.Text>
							{isOpen ?
								<Button variant="primary" onClick={enroll}>Enroll</Button>
								:
								<Button variant="primary" disabled>Enroll</Button>
							}
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}

// Data checker
Course.propTypes = {
	 // Shape() method is used to check if the prop objects conforms to a specific shape
	 courseProp: PropTypes.shape({
	 	// Define Properties and expected type
	 	name: PropTypes.string.isRequired,
	 	description: PropTypes.string.isRequired,
	 	price: PropTypes.number.isRequired
	 })
} 